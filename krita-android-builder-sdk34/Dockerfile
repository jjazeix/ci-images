FROM invent-registry.kde.org/sysadmin/ci-images/krita-appimage-builder

LABEL org.opencontainers.image.title="Linux Image for Krita Android builds"
LABEL org.opencontainers.image.authors="dimula73@gmail.com"

USER root

# install Java necessary for Android SDK

RUN apt-get update && \
    apt-get install -y openjdk-17-jdk && \
    apt-get install -y unzip

# install Android SDK and Android NDK

ENV ANDROID_ROOT=/opt/android-tooling
ENV KDECI_ANDROID_SDK_ROOT=$ANDROID_ROOT/sdk \
    KDECI_ANDROID_NDK_ROOT=$ANDROID_ROOT/android-ndk-r27c/ \
    ANDROID_HOME=$ANDROID_ROOT/sdk \
    PATH="$ANDROID_ROOT/sdk/platform-tools/:$ANDROID_ROOT/cmdline-tools/bin/:$PATH"

WORKDIR $ANDROID_ROOT

#COPY android-ndk-r22b-linux-x86_64.zip commandlinetools-linux-11076708_latest.zip $ANDROID_ROOT/

RUN wget https://dl.google.com/android/repository/android-ndk-r27c-linux.zip && \
    wget https://dl.google.com/android/repository/commandlinetools-linux-11076708_latest.zip &&\
    unzip ./android-ndk-*.zip && \
    unzip ./commandlinetools-linux-*.zip && \
    rm -f *.zip

RUN yes | sdkmanager --sdk_root=$KDECI_ANDROID_SDK_ROOT --licenses && \
    sdkmanager --sdk_root=$KDECI_ANDROID_SDK_ROOT platform-tools && \
    sdkmanager --sdk_root=$KDECI_ANDROID_SDK_ROOT "platforms;android-34" && \
    sdkmanager --sdk_root=$KDECI_ANDROID_SDK_ROOT "build-tools;30.0.3" && \
    sdkmanager --sdk_root=$KDECI_ANDROID_SDK_ROOT "build-tools;34.0.0" && \
    sdkmanager --sdk_root=$KDECI_ANDROID_SDK_ROOT emulator && \
    sdkmanager --sdk_root=$KDECI_ANDROID_SDK_ROOT tools

# install Python 3.10 and all necessary modules

RUN add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update && \
    apt-get install -y python3.10 python3.10-venv

USER appimage
WORKDIR /home/appimage

RUN python3.10 -m venv --upgrade-deps /home/appimage/PythonEnv
ENV PATH="/home/appimage/PythonEnv/bin:$PATH" \
    VIRTUAL_ENV="/home/appimage/PythonEnv"

RUN git clone https://invent.kde.org/dkazakov/krita-deps-management.git && \
    pip install -r krita-deps-management/requirements.txt && \
    rm -rf krita-deps-management
