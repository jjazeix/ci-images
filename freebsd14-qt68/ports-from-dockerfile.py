#!/usr/bin/env python3

import sys

file_path = "Dockerfile"
if len(sys.argv) > 1:
    file_path = sys.argv[1]

interesting_lines_started = False
ports = set()
try:
    with open(file_path, 'r') as file:
        for line in file:
            if line.strip().endswith("pkg install --yes \\"):
                interesting_lines_started = True
                continue
            if '#' in line:
                continue
            if not ("\\" in line):
                interesting_lines_started = False
            if interesting_lines_started:
                ports.add(line.strip(" \n\t\\"))
except FileNotFoundError:
    print(f"File not found: {file_path}")
except Exception as e:
    print(f"An error occurred: {e}")

ports = list(ports)
ports.sort()
for p in ports:
    print(p)
