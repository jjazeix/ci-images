FROM docker.io/alpine:edge

RUN apk update

# Base packages
RUN apk add alpine-sdk \
            # Alpine by default doesn't have Bash but busybox sh
            bash \
            # Additional compilers used for various bits of integration (clazy, kdevelop)
            # ccache is used to speed up builds as well for large projects
            clang ccache \
            # Additional linker
            mold \
            # Of course we need CMake for everything
            cmake \
            # Additional build systems (non-cmake)
            ninja meson \
            # Pip: needed to install various Python modules
            py3-pip \
            # Python bindings for accessibility automation
            py3-atspi \
            # Sphinx documentation tooling for ECM docs
            py3-sphinx \
            # Ruby for various places
            ruby-dev libffi-dev \
            # Utilities to bring up a headless X server instance
            xvfb-run openbox \
            # Utilities to validate Appstream metadata
            appstream \
            # For building documentation tarballs
            bzip2 \
            # For image thumbnails for the KDE.org/applications subsite
            imagemagick \
            # Useful tools for static analysis
            clazy cppcheck py3-codespell \
            # Needed for API Documentation generation
            py3-gv graphviz-dev qt6-qttools-dev doxygen \
            # Needed for some unit tests to function correctly
            hicolor-icon-theme \
            # Needed for some projects that use the non-standard catch2 Unittest mechanisms.
            catch2 \
            # dbus-launch is required for a lot of unit tests
            dbus-x11 \
            # Of course we need Qt, we just install all packages
            qt6-qt3d-dev \
            qt6-qt5compat-dev \
            qt6-qtbase-dev \
            qt6-qtbase-private-dev \
            qt6-qtcharts-dev \
            qt6-qtconnectivity-dev \
            qt6-qtdatavis3d-dev \
            qt6-qtdeclarative-dev \
            qt6-qtdeclarative-private-dev \
            qt6-qthttpserver-dev \
            qt6-qtimageformats-dev \
            qt6-qtlanguageserver-dev \
            qt6-qtlocation-dev \
            qt6-qtlottie-dev \
            qt6-qtmqtt-dev \
            qt6-qtmultimedia-dev \
            qt6-qtnetworkauth-dev \
            qt6-qtpositioning-dev \
            qt6-qtquick3d-dev \
            qt6-qtquicktimeline-dev \
            qt6-qtremoteobjects-dev \
            qt6-qtscxml-dev \
            qt6-qtsensors-dev \
            qt6-qtserialbus-dev \
            qt6-qtshadertools-dev \
            qt6-qtspeech-dev \
            qt6-qtsvg-dev \
            qt6-qttools-dev \
            qt6-qtvirtualkeyboard-dev \
            qt6-qtwayland-dev \
            qt6-qtwebchannel-dev \
            qt6-qtwebengine-dev \
            qt6-qtwebsockets-dev \
            qt6-qtwebview-dev
# Install components needed for the CI tooling to operate (python-gitlab, gcovr, cppcheck_codequality) as well as other CI jobs (check-jsonschema)
# as well as reuse (for unit tests), doxyqml (for building QML documentation) and cheroot/wsgidav/ftpd (for KIO unit tests)
# We also bring in chai and pygdbmi which is used by DrKonqi unit tests
RUN pip install --break-system-packages python-gitlab gcovr cppcheck_codequality reuse doxyqml cheroot wsgidav check-jsonschema chai pygdbmi \
    yamllint==1.33.0
RUN gem install ftpd
# KDE stuff also depends on the following
RUN apk add \
            # kde-builder
            py3-yaml py3-setproctitle py3-pip \
            # modemmanager-qt
            modemmanager-dev \
            # networkmanager-qt
            networkmanager-dev \
            # kcoreaddons
            lsof \
            # kauth
            polkit-dev \
            # kwindowsystem
            xcb-util-keysyms-dev xcb-util-wm-dev weston weston-backend-headless \
            # karchive
            zstd-dev \
            # prison
            libdmtx-dev libqrencode-dev \
            # kimageformats
            openexr-dev libavif-dev libheif-dev libraw-dev libjxl-dev \
            # kwayland and kwin
            wayland-dev \
            wayland-protocols \
            libdisplay-info-dev \
            libei-dev \
            mesa-utils \
            # baloo/kfilemetadata (some for okular)
            attr-dev exiv2-dev taglib-dev poppler-qt5-dev lmdb-dev \
            # kdoctools
            perl-uri docbook-xml docbook-xsl libxml2-dev libxml2-utils libxslt-dev \
            # kio
            acl-dev \
            # various projects need OpenSSL
            openssl-dev \
            # kdnssd
            avahi-dev \
            # khelpcenter and pim
            xapian-core-dev \
            # sonnet
            hunspell-dev \
            # kio-extras and krdc, kio-fuse
            libssh-dev fuse3-dev \
            # plasma-pa
            pulseaudio-qt-dev libcanberra-dev pipewire-pulse \
            # sddm-kcm
            libxcursor-dev \
            # plasma-workspace
            libxtst-dev \
            xdotool \
            # breeze-plymouth
            plymouth-dev \
            # kde-gtk-config/breeze-gtk
            gsettings-desktop-schemas-dev gtk4.0-dev gtk+3.0-dev gtk+2.0-dev sassc \
            # plasma-desktop/discover
            appstream-dev \
            libapk-qt-dev \
            fwupd-dev \
            flatpak-dev flatpak \
            mesa-dri-gallium \
            # plasma-desktop
            xf86-input-evdev-dev xf86-input-libinput-dev xf86-input-synaptics-dev libxkbfile-dev xdg-user-dirs shared-mime-info sdl2-dev \
            # libksane
            sane-dev \
            # pim
            libical-dev xerces-c-dev \
            # <misc>
            alsa-lib-dev fftw-dev \
            # choqok
            qtkeychain-dev \
            # krita
            eigen-dev opencolorio-dev libunibreak-dev quazip-dev \
            # kaccounts
            libaccounts-qt-dev \
            libaccounts-glib-dev \
            signond-dev \
            intltool \
            # skrooge
            sqlcipher-dev sqlite-dev \
            # kwin
            libepoxy-dev mesa-dev mesa-gbm weston libxcvt-dev xwayland libxi-dev libxkbcommon-dev \
            # kgraphviewer
            graphviz-dev \
            # kcalc
            mpfr-dev \
            # kdevelop
            gdb \
            # labplot
            gsl-dev libcerf-dev hdf5-dev netcdf-dev liborcus-dev \
            # digikam
            exiftool \
            # wacomtablet
            libwacom-dev \
            xf86-input-wacom-dev \
            # rust-qt-binding-generator
            rust \
            cargo \
            # kdevelop
            clang \
            clang17-dev \
            llvm17-dev \
            python3-dev \
            # libkleo
            gpgme-dev \
            # akonadi
            mariadb-dev qt6-qtbase-mysql \
            # libkdegames
            openal-soft-dev \
            libsndfile-dev \
            # kscd
            # audiocd-kio
            cdparanoia-dev \
            # ark
            libarchive-dev libzip-dev \
            # k3b
            flac-dev \
            libmad-dev \
            lame-dev \
            libvorbis-dev \
            libsamplerate-dev \
            # kamera
            libgphoto2-dev \
            mlt-dev \
            rttr-dev \
            # print-manager
            cups-dev \
            # krfb
            libvncserver-dev \
            # kscd
            libdiscid-dev \
            # minuet
            fluidsynth-dev \
            # kajongg
            py3-twisted \
            # okular
            djvulibre-dev \
            # ksmtp tests
            cyrus-sasl-dev \
            # kdb
            mariadb-dev postgresql-dev \
            # calligra, krita and probably other things elsewhere too
            boost-dev \
            # Amarok
            gmock gtest gtest-dev curl-dev libgpod-dev libmtp-dev loudmouth-dev \
            mariadb-dev \
            # Cantor
            libspectre-dev \
            py3-numpy \
            py3-matplotlib \
            octave \
            libqalculate-dev \
            # KPat
            freecell-solver-dev black-hole-solver-dev \
            # RKWard
            R-dev \
            # Kaffeine
            v4l-utils-dev \
            vlc-dev \
            # Keysmith
            libsodium-dev \
            # plasma-mobile
            libphonenumber-dev \
            # kquickcharts
            pipewire pipewire-dev \
            # Spectacle
            kimageannotator-dev kcolorpicker-dev \
            # upnp-lib-qt
            kdsoap-qt6-dev \
            # KSysGuard
            libnl3-dev \
            # ksystemstats
            lm-sensors-dev \
            # kitinerary, qrca
            zxing-cpp-dev \
            # ki18n
            iso-codes-dev iso-codes-lang \
            # NeoChat
            qcoro-dev \
            libquotient-dev \
            cmark-dev \
            # KWave
            audiofile-dev \
            # elf-dissector
            libdwarf-dev \
            # plasma-pass
            oath-toolkit-dev \
            # Krita
            libmypaint-dev libheif-dev \
            # Skanpage
            tesseract-ocr-dev leptonica-dev \
            # kup
            libgit2-dev \
            # plasma-nm
            mobile-broadband-provider-info \
            # Spacebar
            c-ares-dev \
            # kxstitch
            imagemagick-dev \
            # plasma-dialer
            callaudiod-dev \
            # kmymoney
            aqbanking-dev \
            # xdg-portal-kde
            gstreamer-dev \	
            # Haruna
            mpv-dev \
            # kscreenlocker
            linux-pam-dev \
            # NeoChat
            olm-dev \
            # Sink/kube
            flatbuffers-dev \
            # KRdc
            freerdp-dev \
            # Glaxnimate
            potrace-dev \
            # selenium-webdriver-at-spi
            py3-opencv \
            # kinfocenter Appium test using selenium-webdriver-at-spi
            wayland-utils \
            # kstars
            wcslib-dev libxisf-dev libnova-dev cfitsio-dev stellarsolver-dev \
            # Kaidan
            libomemo-c-dev \
            qxmpp-dev \
            # Marknote
            md4c-dev \
            # kcodecs
            gperf \
            # Solid
            flex bison \
            # kwallet
            qca-qt6-dev \
            # kpipewire
            pipewire-tools \
            # oxygen-icons
            fdupes \
            # kwidgetsaddons
            font-noto \
            # plasma-workspace
            tzdata \
            # karp
            qpdf-dev \
            # python bindings
            pyside6-dev py3-build py3-pyside6 py3-shiboken6

RUN ln -s /usr/share/zoneinfo/UTC /etc/localtime

# Configure a Flatpak repo (doesn't matter which, but Flathub is easiest)
# Required for Discover unit tests
RUN flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# For D-Bus to be willing to start it needs a Machine ID
RUN dbus-uuidgen > /etc/machine-id
# Certain X11 based software is very particular about permissions and ownership around /tmp/.X11-unix/ so ensure this is right
RUN mkdir /tmp/.X11-unix/ && chown root:root /tmp/.X11-unix/ && chmod 1777 /tmp/.X11-unix/
# We need a user account to do things as, and we need specific group memberships to be able to access video/render DRM nodes
RUN addgroup --gid 44 host-video
RUN addgroup --gid 109 host-render
RUN adduser --home /home/user/ --uid 1000 --disabled-password --ingroup user --ingroup video --ingroup host-render --shell /usr/bin/bash user

# Switch to our unprivileged user account
USER user

